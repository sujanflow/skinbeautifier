//
//  PhotoEditingViewController.swift
//  SkinBeautifierFilters
//
//  Created by Gaetano La delfa on 08/07/16.
//  Copyright © 2016 GAETANO. All rights reserved.
//

import UIKit
import Photos
import PhotosUI

class PhotoEditingViewController: UIViewController, PHContentEditingController {
    
    //CHANGE THE NAME WITH YOURS. IT SHOULD BE: GROUP.NAME_OF_YOUR_ORGANIZATION(AS IN THE TARGET).NAME_OF_THE_APP
    let groupNameForExtension = "group.iftikher.hero1"
    
    //the extension method give us this. It contains all the data related to the image
    var input: PHContentEditingInput = PHContentEditingInput()

    //Source image in DisplaySize
    var inputCIImage: CIImage!
    var sourceImage: UIImage! {
        didSet {
            //I have to put it in a CIImage for the elaboration
            self.inputCIImage = CIImage(cgImage: self.sourceImage.cgImage!)
        }
    }

    //Source image in Full Size
    var inputCIImageFullSize: CIImage!
    var sourceImageFullSize: UIImage! {
        didSet {
            //I have to put it in a CIImage for the elaboration
            self.inputCIImageFullSize = CIImage(cgImage: self.sourceImageFullSize.cgImage!)
        }
    }
    //orientation of the image
    var imageOrientation: Int32?
    
    //This is the place where I will save the processed image with, eventually, the watermark
    var processedImage: UIImage?
    var processedImageWithoutWatermark = UIImage()

    
    //-------------------//
    //--- SKIN FILTER ---//
    //-------------------//
    let context = CIContext(options: [kCIContextWorkingColorSpace: CGColorSpaceCreateDeviceRGB()])
    let filter = YUCIHighPassSkinSmoothing()
  
    //---------------//
    //--- OUTLETS ---//
    //---------------//
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var hueSlider: UISlider!
    
    //-------------------------//
    //--- LIFECYCLE METHODS ---//
    //-------------------------//
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //set the contentMode
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //------------------//
    //--- SCROLLVIEW ---//
    //------------------//
    
    //MARK: ScrollView delegate method
    func viewForZoomingInScrollView(_ scrollView: UIScrollView) -> UIView? {
        return self.photoImageView
    }
    
    
    //--------------//
    //--- SLIDER ---//
    //--------------//
    
    //Repeat processing on the image every time I move the slider.
    @IBAction func amountHueSliderTouchUp(_ sender: AnyObject) {
        self.processImage(self.inputCIImage, scale: self.sourceImage.scale, orientation: self.imageOrientation!, fullSize: false)
        
        self.photoImageView.contentMode = .scaleAspectFit
        self.photoImageView.image = self.processedImage
    }
    
    //--------------------//
    //--- APPLY FILTER ---//
    //--------------------//
    
    //Core function. It apply the custom filter based on the amount of the slider and save the result in the output variable processedImage
    func processImage(_ image:CIImage,scale:CGFloat, orientation:Int32, fullSize:Bool) {
        
        //the input image is filtered
        self.filter.inputImage = image
        self.filter.inputAmount = self.hueSlider.value as NSNumber!
        self.filter.inputRadius = NSNumber(value: Float(7.0 * image.extent.width/750.0))
        self.filter.inputSharpnessFactor = 0.6
        
        //I have to put this to manage the rotation of the image
        if (fullSize) {
            let outputCIImage = filter.outputImage!.oriented(forExifOrientation: self.imageOrientation!)
            let outputCGImage = self.context.createCGImage(outputCIImage, from: outputCIImage.extent)
            let outputUIImage = UIImage(cgImage: outputCGImage!)
            self.processedImage = outputUIImage
        }
        else {
         let outputCIImage = filter.outputImage!
         let outputCGImage = self.context.createCGImage(outputCIImage, from: outputCIImage.extent)
         let outputUIImage = UIImage(cgImage: outputCGImage!, scale: scale, orientation: self.sourceImage.imageOrientation)
         self.processedImage = outputUIImage
        }
    }

    
    //-------------------------//
    //--- EXTENSION METHODS ---//
    //-------------------------//

    func canHandle(_ adjustmentData: PHAdjustmentData) -> Bool {
        // Inspect the adjustmentData to determine whether your extension can work with past edits.
        // (Typically, you use its formatIdentifier and formatVersion properties to do this.)
        return false
    }

    //Here I have the image from the photo library
    func startContentEditing(with contentEditingInput: PHContentEditingInput, placeholderImage: UIImage) {

        input = contentEditingInput
        //save the Image in the attribute I declared. Just the image in a displaySize quality. When done, I will apply the algorithm to the final image
        self.sourceImage = input.displaySizeImage
        self.imageOrientation = input.fullSizeImageOrientation
        //show it
        self.photoImageView.image = self.sourceImage
    }

    //Here I finished the processing => I want to apply the final filter to the fullsize image
    func finishContentEditing(completionHandler: @escaping (PHContentEditingOutput?) -> Void) {
        //Update UI to reflect that editing has finished and output is being rendered. In this case, disable the slider
        hueSlider.isEnabled = false
        //Render and provide output on a background queue.
        
        DispatchQueue.global(qos: .default).async {
       
            //Create editing output from the editing input.
            let output = PHContentEditingOutput(contentEditingInput: self.input)
            let archivedData = NSKeyedArchiver.archivedData(
                withRootObject: "specialSmoothFilter.LDGIngegneria")
            output.adjustmentData = PHAdjustmentData(formatIdentifier: "specialSmoothFilter", formatVersion: "1.0", data: archivedData)
            
            //Take the full size image
            let url = self.input.fullSizeImageURL
            if let imageUrl = url {
                self.sourceImageFullSize = UIImage(contentsOfFile:imageUrl.path)
                //Apply the filter to the fullsize image
                self.processImage(self.inputCIImageFullSize, scale: self.sourceImageFullSize.scale, orientation:self.imageOrientation!,fullSize: true)
                
                if UserDefaults(suiteName: self.groupNameForExtension)?.bool(forKey: "removeAdsAndWatermarks") == false {
                    self.processedImage = ImageManipulationsExtension.addWatermark(self.processedImage!, watermarkImage: UIImage(named: "watermarkExtension.png")!)
                }
                
                
                let renderedJPEGData = UIImageJPEGRepresentation(self.processedImage!, 1.0)
                //2d - Here we are saving our edited image to the URL that is dictated by the content editing output class
                let saveSucceeded = (try? renderedJPEGData!.write(to: output.renderedContentURL, options: [.atomic])) != nil
                if saveSucceeded {
                     completionHandler(output)
                } else {
                    completionHandler(nil)
                }
            }
            else {
                print("An error occurred loading the input image")
                completionHandler(nil)
            }
        }
    }

    var shouldShowCancelConfirmation: Bool {
        // Determines whether a confirmation to discard changes should be shown to the user on cancel.
        // (Typically, this should be "true" if there are any unsaved changes.)
        return false
    }

    func cancelContentEditing() {
        // Clean up temporary files, etc.
        // May be called after finishContentEditingWithCompletionHandler: while you prepare output.
    }

}
