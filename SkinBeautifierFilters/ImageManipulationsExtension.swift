//
//  ImageManipulationsExtension.swift
//  SkinBeautifier
//
//  Created by Gaetano La delfa on 08/07/16.
//  Copyright © 2016 GAETANO. All rights reserved.
//

import Foundation
import UIKit

class ImageManipulationsExtension {
//MARK: add watermark
class func addWatermark(_ originalImage: UIImage, watermarkImage:UIImage) -> UIImage {
    //1 - create a rect with the same size of the original image
    let rect = CGRect(x: 0, y: 0, width: originalImage.size.width, height: originalImage.size.height)
    
    //2 - Creates a bitmap-based graphics context
    UIGraphicsBeginImageContextWithOptions(originalImage.size, true, 0)
    let context = UIGraphicsGetCurrentContext()
    
    //3 - Paint the area of the rect with the specified color (white)
    context?.setFillColor(UIColor.white.cgColor)
    context?.fill(rect)
    
    //4 - paint the original image onto the rect
    originalImage.draw(in: rect, blendMode: .normal, alpha: 1)
    
    //5 - paint the watermark image onto a specified area
    watermarkImage.draw(in: CGRect(x: originalImage.size.width*0.01,y: originalImage.size.height - originalImage.size.width/3,width: originalImage.size.width*0.98,height: originalImage.size.width/3), blendMode: .normal, alpha: 0.6)
    
    //get the result image
    let resultImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return resultImage!
    }
}

