#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "TACloseButton.h"
#import "TAITunesClient.h"
#import "TAPromotee.h"
#import "TAPromoteeApp.h"
#import "TAPromoteeViewController.h"

FOUNDATION_EXPORT double TAPromoteeVersionNumber;
FOUNDATION_EXPORT const unsigned char TAPromoteeVersionString[];

