//
//  Settings.swift
//  SkinBeautifier
//
//  Created by Gaetano Carmelo La Delfa on 05/05/2017.
//  Copyright © 2017 GAETANO. All rights reserved.
//

import Foundation

//photo editor setting: here you can change the appearance of the photo editor (the one that you open to modify the image with stickers, effect etc). Play a bit with the colors to find the right combination for you.
let editorBackgroundColor = UIColor(red: 17.0/255, green: 34.0/255, blue: 51.0/255, alpha: 1.0)
let editorToolbarColor = UIColor(red: 17.0/255, green: 34.0/255, blue: 51.0/255, alpha: 0.8)
let editorToolbarTextColor = UIColor.white

let editorThemeStatusBarStyle = UIStatusBarStyle.lightContent
let navigationBarStyle: UIBarStyle = .black

let appName = "FixPhoto"

//Interstitial ADMOB. Place here the id of your admob unit. The app for user experience reasons, use only interstitial admob
let admobInterstitialUnitID = "ca-app-pub-3940256099942544/4411468910"
//put the Identifiers of your test devices. To know more about this go to the following link:
//http://stackoverflow.com/questions/27871953/where-do-i-find-test-device-id-for-admob
//Using test devices, you can avoid to click on your own Interstitial ADS. NEVER CLICK ON YOUR INTERSTITIAL ADS, GOOGLE WILL BAN YOU FOREVER
let testDevice1 = "5982c32c2658c66d48f0f701577a92ae"
let testDevice2 = "6b5d2277483ae28ec025ac2ca859b825"
let testDevice3 = "5982c32c2658c66d48f0f701577a92ae"





//put here your email. This email will be used for contacting you
let email = "your_email@gmail.com"

//Customize the slider on the bottom of the main screen
let thumbColorOfSlider = UIColor.white
let thicknessOfSlider = 1.0
let thumbSizeOfSlider = 23.0

//Cross promote your other apps, in a simple way. Just put here the IDs of your other apps and the app will fetch the data automatically from the app store
let idApp1 = 111111
let idApp2 = 222222
let idApp3 = 333333
let idApp4 = 444444
let idApp5 = 555555

//IN-APP Purchase: here you need to put the identifier you created in itunes connect, for the in-app purchase product
let inAppProductIdentifier = "com.iftikher.hero1.pro"

//Customize the cell of the tableview (in the setting view): font and font Size
let fontName = "HelveticaNeue-Light"
let fontSize:CGFloat = 17


//Rate us

let iTuneLink = "https://itunes.apple.com/us/app/fix-photo-s-blur-ry-cool-edit-for-pictures-effects/id1174914570?ls=1&mt=8"
//Send Feebback

let feedbackMail = "tridentappsios@gmail.com"

//This is the link to your support webpage. You can put here also the link of your facebook page, or whatever you want. It is used when the user want to share the app by email
let linkToYourPage = "https://www.facebook.com/Fix-Photos-348054642276251/"

//CHANGE THE NAME WITH YOURS. IT SHOULD BE: GROUP.NAME_OF_YOUR_ORGANIZATION(AS IN THE TARGET).NAME_OF_THE_APP
let groupNameForExtension = "group.iftikher.hero1"



