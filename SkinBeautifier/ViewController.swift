//
//  ViewController.swift
//  SkinBeautifier
//
//  Created by Gaetano La delfa on 07/05/16.
//  Copyright © 2016 GAETANO. All rights reserved.
//

import UIKit
import GoogleMobileAds



class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate,UIDocumentInteractionControllerDelegate, GADInterstitialDelegate,GADBannerViewDelegate, PhotoTweaksViewControllerDelegate,CLImageEditorDelegate, CLImageEditorThemeDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    //---------------//
    //--- OUTLETS ---//
    //---------------//
    @IBOutlet weak var hueSlider: GradientSlider!
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var sliderView: UIView!
    
    
    @IBOutlet weak var tapTwiceLabel: UILabel!
    
    @IBOutlet weak var outputReadyLabel: UILabel!
    @IBOutlet weak var effectsButton: UIButton!
    @IBOutlet weak var readyButton: UIButton!
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var bottomCollectionView: UICollectionView!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    //ADMOB interstitial
    var interstitial: GADInterstitial!

    var isRemoveAd = false
    
    var iconImages: [UIImage] = [UIImage(named: "home.png")!,
                                 UIImage(named: "beautify.png")!,
                                 UIImage(named: "edit.png")!,
                                 UIImage(named: "save.png")!,
                                 UIImage(named: "settings-1.png")!]
    
    // Gallery - Camera picker
    let imagePicker = UIImagePickerController()
    var documentController: UIDocumentInteractionController!
    
    //for the skin filter
    let context = CIContext(options: [kCIContextWorkingColorSpace: CGColorSpaceCreateDeviceRGB()])
    let filter = YUCIHighPassSkinSmoothing()
    
    
    //This is the place where I will save the processed image without watermark
    var processedImageWithoutWatermark = UIImage()
    
    var imageOrientation: UIImageOrientation = .up

    
    var minimumZoomScale:CGFloat = 1.0
    var maximumZoomScale:CGFloat = 1.0
    
    
    //This is the place where I will save the processed image with, eventually, the watermark
    var processedImage: UIImage!
//    {
//
//        didSet {
//            //I have to put it in a CIImage for the elaboration
//            self.inputCIImage = CIImage(cgImage: self.processedImage.cgImage!)
//        }
//    }
    //this is the place where I load the source image (a sort of image container)
    var sourceImage: UIImage!
//    var sourceImage: UIImage! {
//        didSet {
//            //I have to put it in a CIImage for the elaboration
//            self.inputCIImage = CIImage(cgImage: self.sourceImage.cgImage!)
//        }
//    }
    var inputCIImage: CIImage!
    
    //The photo editor
    var editor = CLImageEditor()
    
    //for the cross-promotion library
    //["App1","App2","App3","App4","App5"]
    let myAppIDs: [Int] = [idApp1, idApp2,idApp3,idApp4,idApp5]
    
    //---------------------------//
    //--- LIFECYCLE FUNCTIONS ---//
    //---------------------------//
    
    /*NOTE: if the key removeAdsAndWatermarks is false it means that:
     
     The key doesn't exist yet (first installation and launch of the app, or you deleted and reinstalled the app) => you didn't buy any product or you bought it but need to restore it.
     
     */
    
    var bottomTitle = ["Home", "Beautify", "Edit","Save","Setting"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //Some configuration
       // configureTapGestureRecognizer()
        configureSlider()
        configureEditor()
        
        self.bottomCollectionView.delegate = self
        self.bottomCollectionView.dataSource = self
        
        
        //Start processing by loading the picked image in the source image container
        scrollView.isScrollEnabled = true
        photoImageView.alpha = 1.0
        
        unlockZoom()
        //process the image using the filter. The saved image is in the variable processedImage
        //reset the filter
        self.hueSlider.value = self.hueSlider.minimumValue
        self.filter.inputAmount = self.hueSlider.value as NSNumber!
        
        self.setImage()
        
        //reset the zoom
        self.scrollView.zoomScale = 1
        //Hide the tapTwice label
       
        //Show the user interface again
        //enableUI(true)
        
        
       
        let appID = getRandomApp()
        //Show the cross-promotion, only if it is not the first launch and the user did not pay to remove the advertising
        if (UserDefaults.standard.bool(forKey: "isFirstLaunchHappened") == true) && (UserDefaults(suiteName: groupNameForExtension)?.bool(forKey: "removeAdsAndWatermarks") == false) && (appID != -1){
            //Show the cross-promotion advertising
//            TAPromotee.show(from: self, appId: appID, caption: NSLocalizedString("Download it right now!",comment:""), completion: {(userAction: TAPromoteeUserAction) -> Void in
//                switch userAction {
//                case TAPromoteeUserAction.didClose:
//                    // The user just closed the add
//                    print("User did click close")
//                case TAPromoteeUserAction.didInstall:
//                    // The user did click on the Install button so here you can for example disable the ad for the future
//                    print("User did click install")
//                }
//            })
            

        }
        
        // Register to receive notification 
        NotificationCenter.default.addObserver(self, selector: #selector(beautifyDone), name: NSNotification.Name(rawValue: "beautifyDone"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(cancelEditor), name: NSNotification.Name(rawValue: "cancelEditor"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(cancelBeautify), name: NSNotification.Name(rawValue: "cancelBeautify"), object: nil)
        

        
        self.navigationController?.setNavigationBarHidden(false, animated:false)
        navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isTranslucent = false
        
        isRemoveAd = UserDefaults.standard.bool(forKey: "isRemoveAd")
        
        if !isRemoveAd {
            
            //ADMOB
            interstitial = createAndLoadInterstitial()
            
            //  let timer = Timer.scheduledTimer(timeInterval: 2 , target: self, selector: #selector(self.showAd), userInfo: nil, repeats: false)
            
            bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
            bannerView.rootViewController = self
            bannerView.delegate = self
            bannerView.load(GADRequest())
            
        }else{
            
            interstitial = nil
            bannerView.removeFromSuperview()
        }
        
        NSLog("isRemove ad %@",  isRemoveAd.description)
        
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        

        

    }
    
//    @objc func showAd() {
//
//        if interstitial.isReady {
//
//            interstitial.present(fromRootViewController: self)
//
//        } else {
//
//            print("Ad wasn't ready")
//        }
//
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //------------------------------------------//
    //--- HELPER FOR THE CROSS-PROMOTION APP ---//
    //------------------------------------------//
    
    func getRandomApp() -> Int {
        //just to avoid the annoying adv every time you launch the app. It will show the adv, but just sometimes
        let value = Int(arc4random_uniform(10))
        if (value > 4) {
        let id = myAppIDs[Int(arc4random_uniform(5))]
        return id
        }
        else {
            return -1
        }
    }
    
    //--------------------------------//
    //--- INITIAL CONFIG FUNCTIONS ---//
    //--------------------------------//
    
    // 1 - Gesture recognizer
    func configureTapGestureRecognizer() {
        //Add a double tap recognizer
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageSourceSheetButtonPressed))
        tap.numberOfTapsRequired = 2
        view.addGestureRecognizer(tap)
    }
    
    // 2 - Slider
    func configureSlider() {
        
        hueSlider.thumbColor = thumbColorOfSlider
        hueSlider.thickness = CGFloat(thicknessOfSlider)
        hueSlider.thumbSize = CGFloat(thumbSizeOfSlider)
    }
    
    // 3 - Configure editor
    func configureEditor() {
       
        //Set the appearance of the editing tools (Set a black theme rather than a white one)
        editor.theme.backgroundColor = editorBackgroundColor
        editor.theme.toolbarColor = editorToolbarColor
        editor.theme.toolbarTextColor = editorToolbarTextColor
        
        editor.theme.statusBarStyle = editorThemeStatusBarStyle
        UINavigationBar.appearance().barStyle = navigationBarStyle
        
    }
    
    //MARK: ACTIONSHEET MANAGEMENT - Activated when I tap twice in the view
    @objc func imageSourceSheetButtonPressed(_ sender: UIView) {
        
        //instantiate the controller
        let alert = UIAlertController(title:NSLocalizedString("", comment:""), message: NSLocalizedString("Please choose the image source", comment:""), preferredStyle: UIAlertControllerStyle.actionSheet)
        
        //Add the actions: OPEN GALLERY
        let openGalleryActionEditMode = UIAlertAction(title: NSLocalizedString("Gallery",comment:"tradurre"), style: .default) { (alert: UIAlertAction!) -> Void in
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.navigationBar.tintColor = UIColor.white
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        //Add the actions: OPEN CAMERA
        let openCameraActionEditMode = UIAlertAction(title: "Camera", style: .default) { (alert: UIAlertAction!) -> Void in
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .camera
            self.imagePicker.cameraCaptureMode = .photo
            self.imagePicker.modalPresentationStyle = .fullScreen
            self.imagePicker.navigationBar.tintColor = UIColor.white
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        //CANCEL
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel",comment:""), style: .cancel) { action -> Void in
            //Just dismiss the action sheet
            print("dismiss the sheet")
            
        }
        
        //present the sheet
        alert.addAction(openGalleryActionEditMode)
        alert.addAction(openCameraActionEditMode)

        alert.addAction(cancelAction)
        alert.modalPresentationStyle = .popover
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0 ,y: self.view.bounds.size.height/2.0, width: 1.0, height: 1.0)
            presenter.permittedArrowDirections = UIPopoverArrowDirection.unknown
        }
        present(alert, animated: true, completion:nil)
    }
    
    //SHARING SHEET MANAGEMENT
//    @IBAction func shareSheet(_ sender: UIBarButtonItem) {
//
//        let saveSheet = UIAlertController(title:NSLocalizedString("Save or Share your photo", comment: ""), message: NSLocalizedString("", comment:""), preferredStyle: UIAlertControllerStyle.actionSheet)
//
//        let share = UIAlertAction(title: NSLocalizedString("Share on social networks",comment:""), style: .default) { (share: UIAlertAction!) -> Void in
//            if self.processedImage != nil {
//                self.shareOnSocialNetworks(self.processedImage!)
//            }
//            else {
//                //An alert to say to the user that there is nothing to share
//                let alert = UIAlertController(title: NSLocalizedString("There is nothing to share.",comment:""), message: NSLocalizedString("Please tap twice on the screen to load a photo",comment:""), preferredStyle: .alert)
//                let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
//                alert.addAction(action)
//                alert.modalPresentationStyle = .popover
//                self.present(alert, animated: true, completion: nil)
//            }
//        }
//
//        let saveToGallery = UIAlertAction(title: NSLocalizedString("Save to the gallery",comment:""), style: .default) { (saveToGallery: UIAlertAction!) -> Void in
//            if self.processedImage != nil {
//                UIImageWriteToSavedPhotosAlbum(self.processedImage!, self, #selector(ViewController.image(_:didFinishSavingWithError:contextInfo:)), nil)
//            }
//            else {
//
//                //An alert to say to the user that there is nothing to share
//                let alert = UIAlertController(title: NSLocalizedString("There is nothing to save.",comment:""), message: NSLocalizedString("Please tap twice on the screen to load a photo",comment:""), preferredStyle: .alert)
//                let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
//                alert.addAction(action)
//                alert.modalPresentationStyle = .popover
//                self.present(alert, animated: true, completion: nil)
//            }
//        }
//
//        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel",comment:""), style: .cancel) { action -> Void in
//            //Just dismiss the action sheet
//            print("dismiss the sheet")
//        }
//
//        saveSheet.addAction(share)
//        saveSheet.addAction(saveToGallery)
//        saveSheet.addAction(cancelAction)
//
//        if let presenter = saveSheet.popoverPresentationController {
//            presenter.barButtonItem = self.navigationItem.rightBarButtonItem
//            presenter.permittedArrowDirections = UIPopoverArrowDirection.unknown
//        }
//        present(saveSheet, animated: true, completion:nil)
//    }
    
    //called when the image is saved (or not) to the gallery
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if error == nil {
            let ac = UIAlertController(title: NSLocalizedString("Saved!",comment:""), message: NSLocalizedString("Your altered image has been saved to your photos.",comment:""), preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: NSLocalizedString("Save error",comment:""), message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        }
    }
    
    //set image in image view
    func setImage() {
        
        self.processedImage = self.sourceImage
        self.processedImageWithoutWatermark = self.processedImage!
        
        self.photoImageView.image = self.processedImage

    }
    
    //Core function. It apply the custom filter based on the amount of the slider and save the result in the output variable processedImage
//    func processImage() {
//
//        self.filter.inputImage = self.inputCIImage
//        self.filter.inputAmount = self.hueSlider.value as NSNumber!
//        self.filter.inputRadius = NSNumber(value: Float(7.0 * self.inputCIImage.extent.width/750.0))
//        self.filter.inputSharpnessFactor = 3.0
//        let outputCIImage = filter.outputImage!
//
//        let outputCGImage = self.context.createCGImage(outputCIImage, from: outputCIImage.extent)
////        let outputUIImage = UIImage(cgImage: outputCGImage!, scale: self.processedImage!.scale, orientation: self.processedImage!.imageOrientation)
//        let outputUIImage = UIImage(cgImage: outputCGImage!, scale: self.sourceImage.scale, orientation: self.sourceImage.imageOrientation)
//
//
////        if UserDefaults(suiteName: groupNameForExtension)?.bool(forKey: "removeAdsAndWatermarks") == false {
////            //Store Image without watermark in a variable, to use it later for the filters
////            self.processedImageWithoutWatermark = outputUIImage
////            //Add watermark to the image
////            self.processedImage = ImageManipulations.addWatermark(outputUIImage, watermarkImage: UIImage(named: "watermark.png")!)
////        }
////        else {
//            self.processedImage = outputUIImage
//            self.processedImageWithoutWatermark = self.processedImage!
////        }
//        self.photoImageView.image = self.processedImage
//    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //Save the orientation of the picked image
        imageOrientation = (info[UIImagePickerControllerOriginalImage] as? UIImage)!.imageOrientation

        photoImageView.contentMode = .scaleAspectFit
        scrollView.isScrollEnabled = true
        photoImageView.alpha = 1.0
        
        //reduce the image size
        let pickedImageReduced = ImageManipulations.reduceImageSize((info[UIImagePickerControllerOriginalImage] as? UIImage)!, maxSizePermitted: 1000)
        
        //re-apply the saved image orientation
        let imageWithCorrectedWithOrientation = UIImage(cgImage: pickedImageReduced.cgImage!, scale: 1.0, orientation: imageOrientation)
        
        //Pass everything to the photo tweak library
        editPhoto(ImageManipulations.reduceImageSize(imageWithCorrectedWithOrientation, maxSizePermitted: 1000), picker: picker)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //Repeat processing on the image every time I move the slider.
    @IBAction func amountHueSliderTouchUp(_ sender: AnyObject) {
        
//        if self.inputCIImage != nil {
//            self.processImage()
//        }
    }
    
    //-------------------//
    //--- PHOTOTWEAKS ---//
    //-------------------//
    
    //photoTweak library settings (It is the library that let us to crop the image, in edit mode)
    
    //MARK: 1 - PhotoTweaks settings
    func editPhoto(_ imageFromPicker:UIImage, picker:UIImagePickerController) {
        
        let photoTweaksViewController = PhotoTweaksViewController(image: imageFromPicker)
        photoTweaksViewController?.delegate = self
        photoTweaksViewController?.autoSaveToLibray = false
        photoTweaksViewController?.maxRotationAngle = CGFloat(Double.pi/4)
        picker.pushViewController(photoTweaksViewController!, animated: true)
    }
    
    // MARK: 2 - PhotoTweaksViewControllerDelegate Methods: SUCCESS
    func photoTweaksController(_ controller: PhotoTweaksViewController!, didFinishWithCroppedImage croppedImage: UIImage!) {
        
        //dismiss the controller
        // _ = means "I am deliberately ignoring the result of this call"
        _ = controller.navigationController?.popViewController(animated: true)
        self.sourceImage = croppedImage
        //Start processing by loading the picked image in the source image container
        scrollView.isScrollEnabled = true
        photoImageView.alpha = 1.0
        
        unlockZoom()
        //process the image using the filter. The saved image is in the variable processedImage
        //reset the filter
        self.hueSlider.value = self.hueSlider.minimumValue
        self.filter.inputAmount = self.hueSlider.value as NSNumber!
        self.setImage()
        //reset the zoom
        self.scrollView.zoomScale = 1
        //Hide the tapTwice label
        //tapTwiceLabel.isHidden = true
        //Show the user interface again
        //enableUI(true)
        
        //Dismiss the picker and present the advertising admob
        dismiss(animated: true, completion: {
            //if the user didn't pay => the "removeAdsAndWatermarks" is false, so we present the ads.
                if UserDefaults(suiteName: groupNameForExtension)?.bool(forKey: "removeAdsAndWatermarks") == false {
                if self.interstitial.isReady {
                    self.interstitial.present(fromRootViewController: self)
                }
            }
        })
    }
    // MARK: 2 - PhotoTweaksViewControllerDelegate Methods: CANCEL
    func photoTweaksControllerDidCancel(_ controller: PhotoTweaksViewController!) {
        //just dismiss the controller
        _ = controller.navigationController?.popViewController(animated: true)
    }
    
    //-----------------------//
    //--- SHARE ON SOCIAL ---//
    //-----------------------//
    
    //MARK: share
    func shareOnSocialNetworks(_ image:UIImage!) {
        
        // 1 - Create the instagram URL
        let instagramURL = URL(string: "instagram://app")
    
        // 2 - if the URL scheme can be handled by some app (instagram in such case)
        if (UIApplication.shared.canOpenURL(instagramURL!)) {
            //Convert the image into data
            let imageData = UIImageJPEGRepresentation(image!, CGFloat(UserDefaults.standard.float(forKey: "imageQuality")))
            let captionString = "SkinBeautifier.jpg"
            let writePath = (NSTemporaryDirectory() as NSString).appendingPathComponent("SkinBeautifier.jpg")
            if ((try? imageData?.write(to: URL(fileURLWithPath: writePath), options: [.atomic])) != nil) == false {
                return
            }
            else {
                //Create the URL
                let fileURL = URL(fileURLWithPath: writePath)
                self.documentController = UIDocumentInteractionController(url: fileURL)
                self.documentController.delegate = self
                self.documentController.uti = "com.instagram.photo"
                self.documentController.annotation = NSDictionary(object: captionString, forKey: "InstagramCaption" as NSCopying)
                //if iPad
                if UIDevice.current.userInterfaceIdiom == .pad {
                    self.documentController.presentOpenInMenu(from: self.navigationItem.rightBarButtonItem!, animated: true)
                }
                else {
                    self.documentController.presentOpenInMenu(from: self.view.frame, in: self.view, animated: true)
                }
            }
        }
            //5 - If the URL scheme cannot be handled, just show an error log
        else {
            print(" Instagram isn't installed ")
        }
    }
    
    //------------------//
    //--- SCROLLVIEW ---//
    //------------------//
    
    //MARK: 1 - ScrollView delegate method
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.photoImageView
    }
    
    //MARK: 2 - Lock and unlock the zoom
    func lockZoom(){
        maximumZoomScale = scrollView.maximumZoomScale
        minimumZoomScale = scrollView.minimumZoomScale
        
        scrollView.maximumZoomScale = 1.0;
        scrollView.minimumZoomScale = 1.0;
    }
    
    func unlockZoom(){
        scrollView.maximumZoomScale = maximumZoomScale
        scrollView.minimumZoomScale = minimumZoomScale
    }
    
    @IBAction func processingImageFinished(_ sender: AnyObject) {
        if self.processedImage != nil {
            
            //enableUI(false)
            self.bottomView.isHidden = false;
            self.sliderView.isHidden = true;
            
        }
        
        else {
            let alert = UIAlertController(title: NSLocalizedString("There is nothing to work on.",comment:"tradurre"), message: NSLocalizedString("Please tap twice on the screen to load a photo",comment:"tradurre"), preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(action)
            alert.modalPresentationStyle = .popover
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    //---------------------------//
    //--- AVIARY MANIPULATION ---//
    //---------------------------//
    
    //MARK: 1 - filters from the editor
    @IBAction func displayEditorForImage(_ sender: UIButton) {
        
//        //If the image exists, open the Aviary pre-made Controller
//        if self.processedImage != nil {
//            editor = CLImageEditor(image: self.processedImageWithoutWatermark, delegate: self)
//            present(editor, animated: true, completion: nil)
//        }
//        else {
//            let alert = UIAlertController(title: NSLocalizedString("There is nothing to work on.",comment:"tradurre"), message: NSLocalizedString("Please tap twice on the screen to load a photo",comment:"tradurre"), preferredStyle: .alert)
//            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
//            alert.addAction(action)
//            alert.modalPresentationStyle = .popover
//            self.present(alert, animated: true, completion: nil)
//
//        }
    }
    
    public func imageEditor(_ editor: CLImageEditor!, didFinishEdittingWith image: UIImage!) {
       // enableUI(false)
        editor.dismiss(animated: true, completion: {
            
            NSLog("didFinishEdittingWith in viewcontroller")
            
            if UserDefaults(suiteName: groupNameForExtension)?.bool(forKey: "removeAdsAndWatermarks") == false {
                //The user didn't pay, so add watermark in the final image
                self.processedImage = ImageManipulations.addWatermark(image!, watermarkImage: UIImage(named: "watermark.png")!)
            }

                //the user payed so don't put watermark in the final image.
            else {
                self.processedImage = image!
            }
            self.photoImageView.image = self.processedImage
            
            //ADMOB
            self.interstitial = self.createAndLoadInterstitial()
            
        })
        
        
    }
    
    
    
    //-------------//
    //--- ADMOB ---//
    //-------------//
    
    //MARK: 1 - AdMob
    func createAndLoadInterstitial() -> GADInterstitial {
        
        let interstitial = GADInterstitial(adUnitID: admobInterstitialUnitID)
        interstitial.delegate = self
        let request = GADRequest()
        request.testDevices = [testDevice1,testDevice2, testDevice3]
        interstitial.load(request)
        return interstitial
    }
    
    //MARK: 2 - AdMob delegate
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        
        print("interstitialDidReceiveAd")
        
        
        interstitial.present(fromRootViewController: self)
        
        
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        
       // interstitial = createAndLoadInterstitial()
        
    }
    
    //------------------------//
    //--- USEFUL FUNCTIONS ---//
    //------------------------//
    
    func enableUI(_ value:Bool) {
        if value == true {
            hueSlider.isHidden = false
            readyButton.isHidden = false
            effectsButton.isHidden = true
 //           outputReadyLabel.isHidden = true
        }
        else {
            hueSlider.isHidden = true
            readyButton.isHidden = true
            effectsButton.isHidden = true
//            outputReadyLabel.isHidden = false
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "tabCell", for: indexPath )
            
            
//
            let title : UILabel = cell.contentView.viewWithTag(202) as! UILabel
            let icon : UIImageView = cell.contentView.viewWithTag(201) as! UIImageView
//
             icon.image = iconImages[indexPath.item]
             title.text = bottomTitle[indexPath.item]
        
            
            return cell
            
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  20
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/5-10, height: 60)
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.item == 0 {
            
            self.navigationController?.popToRootViewController(animated: true)
            
        }else if indexPath.item == 1{
            
            let destViewController = storyboard?.instantiateViewController(withIdentifier: "BeautiFyViewController") as! BeautiFyViewController
            
            destViewController.sourceImage = self.photoImageView.image
            
            self.navigationController!.present(destViewController, animated: true,completion: nil)
            
            
        }else if indexPath.item == 2{
            
            //If the image exists, open the Aviary pre-made Controller
            if self.processedImage != nil {
                
                editor = CLImageEditor(image: self.processedImage, delegate: self)
                present(editor, animated: true, completion: nil)
                
            }
            else {
                let alert = UIAlertController(title: NSLocalizedString("There is nothing to work on.",comment:"tradurre"), message: NSLocalizedString("Please tap twice on the screen to load a photo",comment:"tradurre"), preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alert.addAction(action)
                alert.modalPresentationStyle = .popover
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }else if indexPath.item == 3{
            
                
                let saveSheet = UIAlertController(title:NSLocalizedString("Save or Share your photo", comment: ""), message: NSLocalizedString("", comment:""), preferredStyle: UIAlertControllerStyle.actionSheet)
                
                let share = UIAlertAction(title: NSLocalizedString("Share",comment:""), style: .default) { (share: UIAlertAction!) -> Void in
                   
                    if self.processedImage != nil {
                        
                        let activityViewController = UIActivityViewController(activityItems: [self.processedImage! as UIImage], applicationActivities: nil)
                        
                        if let popoverPresentationController = activityViewController.popoverPresentationController {
                            
                            popoverPresentationController.sourceView = self.view
                            
                            var temp: CGRect = UIScreen.main.bounds
                            temp.size.height = temp.size.height / 2
                            popoverPresentationController.sourceRect = temp
                        }

                        
                        self.present(activityViewController, animated: true, completion: {})
                        
                        
                        
                        
                    }
                    else {
                        //An alert to say to the user that there is nothing to share
                        let alert = UIAlertController(title: NSLocalizedString("There is nothing to share.",comment:""), message: NSLocalizedString("Please load a photo",comment:""), preferredStyle: .alert)
                        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        alert.addAction(action)
                        alert.modalPresentationStyle = .popover
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                let saveToGallery = UIAlertAction(title: NSLocalizedString("Save to the gallery",comment:""), style: .default) { (saveToGallery: UIAlertAction!) -> Void in
                    if self.processedImage != nil {
                        UIImageWriteToSavedPhotosAlbum(self.processedImage!, self, #selector(ViewController.image(_:didFinishSavingWithError:contextInfo:)), nil)
                    }
                    else {
                        
                        //An alert to say to the user that there is nothing to share
                        let alert = UIAlertController(title: NSLocalizedString("There is nothing to save.",comment:""), message: NSLocalizedString("Please load a photo",comment:""), preferredStyle: .alert)
                        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        alert.addAction(action)
                        alert.modalPresentationStyle = .popover
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel",comment:""), style: .cancel) { action -> Void in
                    //Just dismiss the action sheet
                    print("dismiss the sheet")
                }
                
                saveSheet.addAction(share)
                saveSheet.addAction(saveToGallery)
                saveSheet.addAction(cancelAction)
            
               if let popoverPresentationController = saveSheet.popoverPresentationController {
                
                  popoverPresentationController.sourceView = self.view
                
                  var temp: CGRect = UIScreen.main.bounds
                  temp.size.height = temp.size.height / 2
                  popoverPresentationController.sourceRect = temp
               }
            
                
                present(saveSheet, animated: true, completion:nil)
            
            
            
        }else if indexPath.item == 4{
            
            let destViewController = storyboard?.instantiateViewController(withIdentifier: "settingNavigation") as! UINavigationController

            self.present(destViewController, animated: true,completion: nil) 
           
            
        }
        
    }
    
    @IBAction func cancelBeautifyButtonAction(_ sender: Any) {
        
        self.bottomView.isHidden = false
        self.sliderView.isHidden = true
    }
    
    @objc func beautifyDone(_ noti: Notification?) {
        
        NSLog("Notification found")
        
        if let dict = noti!.userInfo as NSDictionary? {
            if let id = dict["image"] as? UIImage{
                
                self.processedImage = id as UIImage
                self.photoImageView.image = id as UIImage
            }
        }
        
        //ADMOB
        interstitial = createAndLoadInterstitial()
        
    }
    
    @objc func cancelEditor(_ noti: Notification?) {
        
        //ADMOB
        interstitial = createAndLoadInterstitial()
        
    }
    
    @objc func cancelBeautify(_ noti: Notification?) {
        
        //ADMOB
        interstitial = createAndLoadInterstitial()
        
    }

    
}
