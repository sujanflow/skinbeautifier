//
//  InAppPurchase.swift
//  SkinBeautifier
//
//  Created by Gaetano La delfa on 02/06/16.
//  Copyright © 2016 GAETANO. All rights reserved.
//

import Foundation
import StoreKit

class InAppPurchase: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    //product identifier we created on iTunes Connect
    let removeAdsAndWatermarksProductIdentifier = inAppProductIdentifier
    var isIAPEnabled = false
    
    //place where we store single product and array of products taken from the itunes connect
    var removeAdsAndWatermarkProduct = SKProduct()
    var removeAdsAndWatermarkProductArray = Array<SKProduct>()
    
    
    func showAlertForIAPDisabled() -> UIAlertController {
        //Show an alert
        let alert = UIAlertController(title: NSLocalizedString("In-App Purchases Not Enabled",comment:"tradurre"), message: NSLocalizedString("Please enable In App Purchase in Settings",comment:"tradurre"), preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { alertAction in
            alert.dismiss(animated: true, completion: nil)
            //and open the settings of the phone
            let url: URL? = URL(string: UIApplicationOpenSettingsURLString)
            if url != nil
            {
                UIApplication.shared.openURL(url!)
            }
        }))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { alertAction in
            alert.dismiss(animated: true, completion: nil)
        }))
        return alert
    }
    
    func showAlertForFailedTransaction() -> UIAlertController {
        
        let alert = UIAlertController(title: NSLocalizedString("Purchase Failed",comment:"tradurre"), message: NSLocalizedString("Please try again",comment:"tradurre"), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:nil))
        return alert
    }
    
    //MARK: PHASE ZERO: request product data.
    //The first thing we have to do, is request the product data to apple servers. We can do this when we load a specific viewController for example the settingsTableViewController
    func requestProductsData() -> Bool {
        
        //setup IAPs. First, check if the user has enabled the in-app purchase
        if SKPaymentQueue.canMakePayments() {
            isIAPEnabled = true
            print("REQUEST PRODUCT DATA: iap is enabled, loading...")
            let productsID = NSSet(objects: removeAdsAndWatermarksProductIdentifier)
            
            //Make the request. The request method needs as input a set of product identifiers
            let request = SKProductsRequest(productIdentifiers: productsID as! Set<String>)
            request.delegate = self
            request.start()
        }
        //if the user has not enabled the iAP, show an alert and propose to go to the settings page of the phone
        else {
            isIAPEnabled = false
            print("REQUEST PRODUCT DATA: iAP disabled, please enable it")
        }
        
        return isIAPEnabled
    }
    
    //MARK: PHASE ONE - product request RESPONSE (SKProductsRequestDelegate)
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        //get the list of products, depending on the product identifier provided in the original request
        let myProducts = response.products
        //There are products. get all the informations
        if (myProducts.count != 0) {
            for product in myProducts {
                
                print("PRODUCTREQUEST_DIDRECEIVERESPONSE: product added")
                print(product.productIdentifier)
                print(product.localizedTitle)
                print(product.localizedDescription)
                print(product.price)
                
                self.removeAdsAndWatermarkProduct = product
                self.removeAdsAndWatermarkProductArray.append(self.removeAdsAndWatermarkProduct)
            }
            //Send a notification to the settingsTableViewController with the payload
            NotificationCenter.default.post(name: Notification.Name(rawValue: "productRequestDidReceiveResponseOK"), object: self)
        }
        else {
            //This, usually can't happen if you correctly set the IAP on itunes connect
            print("PRODUCTREQUEST_DIDRECEIVERESPONSE: no product found")
        }
        
        for product in response.invalidProductIdentifiers
        {
            //This, usually can't happen if you correctly set the IAP on itunes connect
            print("PRODUCTREQUEST_DIDRECEIVERESPONSE: Product not found: \(product)")
        }
    }
    
    //the product is: org.LDGIngegneria.SkinBeautifier
    //MARK: PHASE TWO - Requesting payment
    func buyProduct() {
        print("CLICK_ON_BUY_BUTTON: buy" + removeAdsAndWatermarkProduct.productIdentifier)
        let payment = SKPayment(product: removeAdsAndWatermarkProduct)
        //register the class with the delegate and add the transaction observer
        SKPaymentQueue.default().add(self)
        //singleton SKPaymentQueue object called defaultQueue(). Here we add the payment
        SKPaymentQueue.default().add(payment)
    }
    
    //Once the user has confirmed they would like to purchase your product, the paymentQueue delegate method is invoked. This method evaluates the state of each transaction in an array of updated transactions
    //MARK: PHASE THREE - delivering products
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        print("PAYMENTQUEUE_UPDATEDTRANSACTIONS: add payment")
        //cycle on all the transactions (actually we can have just one transaction)
        for transaction in transactions {
            //for each transaction lets examinate the transaction state
            switch transaction.transactionState {
            //1 - payment was processed with success
            case .purchased:
                print("PAYMENTQUEUE_UPDATEDTRANSACTIONS: buy ok, remove watermark and ads")
                
                UserDefaults.standard.set(true, forKey: "isRemoveAd")
                
                print(removeAdsAndWatermarkProduct.productIdentifier)
                //I have to understand which is the product the user bought
                let productID = removeAdsAndWatermarkProduct.productIdentifier
                switch productID {
                case removeAdsAndWatermarksProductIdentifier:
                    
                    print("remove ads and watermark")
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "transactionOK"), object: self)
                default:
                    print("IAP not setup")
                    let alert = UIAlertController(title: NSLocalizedString("Purchase Failed",comment:"tradurre"), message: NSLocalizedString("Please try again",comment:"tradurre"), preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { alertAction in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    break
                }
                //close the transaction by removing it from the queue
                queue.finishTransaction(transaction)
            //2 - transaction failed for some reasons that you can read on the error
            case .failed:
                print("buy error")
                queue.finishTransaction(transaction)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "transactionFailed"), object: self)
            case .restored:
                
                queue.finishTransaction(transaction)
                UserDefaults.standard.set(true, forKey: "isRemoveAd")
                
                
            default:
                break
            }
        }
    }
    
    //Delegate for restore
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        print("PAYMENTQUEUERESTORECOMPLETEDTRANSACTIONSFINISHED: transaction restore finish")
        for transaction in queue.transactions {
            let productID = transaction.payment.productIdentifier
            switch productID {
            case removeAdsAndWatermarksProductIdentifier:
                print("PAYMENTQUEUERESTORECOMPLETEDTRANSACTIONSFINISHED: remove ads and watermark")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "transactionOK"), object: self)
            default:
                print("IAP not setup")
                NotificationCenter.default.post(name: Notification.Name(rawValue: "transactionFailed"), object: self)
                break
            }
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, removedTransactions transactions: [SKPaymentTransaction]) {
        print("remove transaction. You can update the UI here")
        //can send a notification to the settings view controller and update the button for example with a label which say: Thanks for buying. Your app is Ads/watermarks free!
    }
    
    func restorePurchases() {
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}
