//
//  SettingsTableViewController.swift
//  SkinBeautifier
//
//  Created by Gaetano La delfa on 18/05/16.
//  Copyright © 2016 GAETANO. All rights reserved.
//

import UIKit
import AcknowList
import Social
import MessageUI
import StoreKit

class SettingsTableViewController: UITableViewController,MFMailComposeViewControllerDelegate {
    
    
    let LOW_IMAGE_QUALITY:Float = 0.01
    let MEDIUM_IMAGE_QUALITY:Float = 0.5
    let HIGH_IMAGE_QUALITY:Float = 1.0
    
    /***************/
    /*** OUTLETS ***/
    /***************/
    @IBOutlet weak var imageQualitySegmentedControl: UISegmentedControl!
    @IBOutlet weak var sharpnessStepper: UIStepper!
    @IBOutlet weak var sharpnessValueLabel: UILabel!
    @IBOutlet weak var removeAdsButton: UIButton!
    @IBOutlet weak var restorePurchaseButton: UIButton!
    
//    /***************************************/
//    /*** IAP MANAGER VARIABLES/CONSTANTS ***/
//    /***************************************/
    let iAP = InAppPurchase()
    let removeAdsAndWatermarksProductIdentifier = inAppProductIdentifier
    var removeAdsAndWatermarkProductArray = Array<SKProduct>()
    
    var tellAFriendSheet = UIAlertController()
    
    //Action connected to the UISegmentedControl: when I change the image quality in the segmented control UI, the correspondent key in UserDefaults changes
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex
        {
        case 0:
            UserDefaults.standard.set(LOW_IMAGE_QUALITY, forKey: "imageQuality")
        case 1:
            UserDefaults.standard.set(MEDIUM_IMAGE_QUALITY, forKey: "imageQuality")
        case 2:
            UserDefaults.standard.set(HIGH_IMAGE_QUALITY, forKey: "imageQuality")
        default:
            break;
        }
    }
    
    //Action connected to the stepper: when I click on it, the label on the left change
    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        let sharpnessValue = String(format: "%.1f", sender.value)
        sharpnessValueLabel.text = sharpnessValue
        UserDefaults.standard.set(Float(sender.value), forKey: "sharpness")
    }
    
    func showTellAFriendActionSheet() {
        
        tellAFriendSheet = UIAlertController(title:NSLocalizedString("Spread the word to your friends!", comment: "tradurre"), message: NSLocalizedString("", comment:"tradurre"), preferredStyle: UIAlertControllerStyle.actionSheet)

        let shareOnTwitter = UIAlertAction(title: NSLocalizedString("Share on Twitter",comment:"tradurre"), style: .default) { (shareOnTwitter: UIAlertAction!) -> Void in
            self.postToTwitter()
        }

        let shareByMail = UIAlertAction(title: NSLocalizedString("Send by E-mail",comment:"tradurre"), style: .default) { (shareByMail: UIAlertAction!) -> Void in
            
            if let image = UIImage(named: "imageForShare.png"){
                self.sendByMail(image)
            }
            else {
                self.sendByMail(nil)
            }
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel",comment:""), style: .cancel) { action -> Void in
            print("dismiss the sheet")
        }
        
        tellAFriendSheet.addAction(shareOnTwitter)
        tellAFriendSheet.addAction(shareByMail)
        tellAFriendSheet.addAction(cancelAction)

    }
    
    //MARK: post to twitter
    func postToTwitter () {
        
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter){
            let twitterSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            twitterSheet.setInitialText(NSLocalizedString("THE TITLE YOU PREFER - ES THE NAME OF THE APP",comment:"tradurre"))
            twitterSheet.add(UIImage(named: "imageForShare.png"))
            twitterSheet.add(URL(string:NSLocalizedString("http://itunes.apple.com/BLABLABLA - IS THE LINK OF YOUR APP, YOU WANT TO SHARE", comment:"tradurre")))
            self.present(twitterSheet, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Accounts", message: NSLocalizedString("Please login to a Twitter account to share.", comment:"tradurre"), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: send mail
    func sendByMail(_ image:UIImage?) {
        if MFMailComposeViewController.canSendMail() {
            let mailer = MFMailComposeViewController()

            mailer.mailComposeDelegate = self
            mailer.setSubject(NSLocalizedString("THE TITLE YOU PREFER - ES THE NAME OF THE APP",comment:"tradurre"))
            mailer.setToRecipients(nil)
            //generate the attachment
            
            let imageData = UIImagePNGRepresentation(image!)
            mailer .addAttachmentData(imageData!, mimeType: "image/png", fileName:"the_name_of_the_attached_file_you_prefer - es skinbeautifier")
            //body of the message, modify it as you prefer
            let emailBodyDescription = NSLocalizedString("Skin Beautifier: the best App to beautify your skin and share the results on Instagram or on the other social networks. Follow us on Facebook:", comment:"tradurre")
            
            //link to the website of your app
            let emailBodyWithWebsiteLink = linkToYourPage
            //link dell'app
            let emailBodyWithAppLink=NSLocalizedString("http://itunes.apple.com/BLABLABLA - IS THE LINK OF YOUR APP, YOU WANT TO SHARE", comment:"tradurre")
            //corpo della mail
            let emailBody = NSLocalizedString("\(emailBodyDescription)\n\n\(emailBodyWithWebsiteLink)\n\nOr AppStore Link:\n\n\(emailBodyWithAppLink)",comment:"tradurre")
            mailer.setMessageBody(emailBody, isHTML: false)
            present(mailer, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Ops!",
                                          message: NSLocalizedString("Your device doesn't support the mail composer sheet", comment:"tradurre"), preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func contactUs() {
        
        if MFMailComposeViewController.canSendMail() {
            let mailer = MFMailComposeViewController()
            mailer.mailComposeDelegate = self
            mailer.setSubject(NSLocalizedString("NAME_OF_YOUR_APP - Request for information", comment:"tradurre"))
            let recipients = [email]
            mailer.setToRecipients(recipients)
            
            let emailBody = NSLocalizedString("Write your message here...",comment:"tradurre")
            mailer.setMessageBody(emailBody, isHTML: false)
            present(mailer, animated: true, completion: nil)
        }
        
    }
    
    //MARK: mailComposer delegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Mail cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Mail saved")
        case MFMailComposeResult.sent.rawValue:
            print("Mail sent")
        case MFMailComposeResult.failed.rawValue:
            let alert = UIAlertController(title: "Ops!",
                                          message: NSLocalizedString("Cannot send email.", comment:"tradurre"), preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        default:
            print("nothing")
        }
        dismiss(animated: true, completion: nil)
    }
    
    //To preserve the selection in the UISegmentedControl
    func setImageQuality() {
        switch UserDefaults.standard.float(forKey: "imageQuality") {
        case LOW_IMAGE_QUALITY:
            imageQualitySegmentedControl.selectedSegmentIndex = 0
        case MEDIUM_IMAGE_QUALITY:
            imageQualitySegmentedControl.selectedSegmentIndex = 1
        case HIGH_IMAGE_QUALITY:
            imageQualitySegmentedControl.selectedSegmentIndex = 2
        default:
            break
        }
    }
    
    //To preserve the value of the stepper
    func setSharpnessValues() {
        sharpnessStepper.value = Double(UserDefaults.standard.float(forKey: "sharpness"))
        let sharpnessValue = String(format: "%.1f", sharpnessStepper.value)
        sharpnessValueLabel.text = sharpnessValue
    }
    
//    func enableBuyButton(_ value:Bool) {
//        if value == true {
//            removeAdsButton.isEnabled = true
//            removeAdsButton.setTitleColor(UIColor(red: 235.0/255, green: 83.0/255, blue: 122.0/255, alpha: 1.0), for: UIControlState())
//        }
//        else {
//            removeAdsButton.isEnabled = false
//            removeAdsButton.setTitleColor(UIColor.lightGray, for: UIControlState())
//        }
//    }
    
//    func enableRestoreButton(_ value:Bool) {
//        if value == true {
//            restorePurchaseButton.isEnabled = true
//            restorePurchaseButton.setTitleColor(UIColor(red: 235.0/255, green: 83.0/255, blue: 122.0/255, alpha: 1.0), for: UIControlState())
//        }
//        else {
//            restorePurchaseButton.isEnabled = false
//            restorePurchaseButton.setTitleColor(UIColor.lightGray, for: UIControlState())
//        }
//    }
    
    
    
    //MARK: viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //1 - SET THE VALUES FOR THE IMAGE QUALITY BY TAKING IT FROM NSUSERDEFAULT
        setImageQuality()
        
        //2 - SET SHARPNESS VALUE BY TAKING IT FROM NSUSERDEFAULT
        setSharpnessValues()
        
//        //3 - ADD OBSERVER FOR THE LIST OF PRODUCTS
//        NotificationCenter.default.addObserver(self, selector: #selector(SettingsTableViewController.manageProductResponse), name: NSNotification.Name(rawValue: "productRequestDidReceiveResponseOK"), object: nil)
//        
//        //3bis - ADD OBSERVER FOR TRANSACTION FAILED
//        NotificationCenter.default.addObserver(self, selector: #selector(SettingsTableViewController.transactionFailed), name: NSNotification.Name(rawValue: "transactionFailed"), object: nil)
//
//        
//        //4 - ADD OBSERVER FOR REMOVING ADS AND WATERMARKS AFTER TRANSACTION IS OK
//        NotificationCenter.default.addObserver(self, selector: #selector(SettingsTableViewController.remove), name: NSNotification.Name(rawValue: "transactionOK"), object: nil)
//        
//        //5 - MAKE THE REQUEST FOR THE LIST OF PRODUCTS
//        if !iAP.requestProductsData() {
//            let alert = iAP.showAlertForIAPDisabled()
//            present(alert, animated: true, completion:nil)
//        }
//        
//        //6 - disable both purchase and restore buttons
//        enableBuyButton(false)
//        enableRestoreButton(false)
//        
        self.navigationController?.navigationBar.isTranslucent = false
        
    }
    
    @IBAction func dismissSettingsViewController(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    
    /*****************/
    /*** TABLEVIEW ***/
    /*****************/
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 2 {
            contactUs()
        }
            
        else if (indexPath as NSIndexPath).section == 1 && (indexPath as NSIndexPath).row == 1 {
            let path = Bundle.main.path(forResource: "Pods-SkinBeautifier-acknowledgements", ofType: "plist")
            let creditViewController = AcknowListViewController(acknowledgementsPlistPath: path)
            self.navigationController?.pushViewController(creditViewController, animated: true)
        }
        else if (indexPath as NSIndexPath).section == 1 && (indexPath as NSIndexPath).row == 2 {
            showTellAFriendActionSheet()
            
            if let presenter = tellAFriendSheet.popoverPresentationController {
                presenter.sourceView = tableView.cellForRow(at: indexPath)
                presenter.sourceRect = (tableView.cellForRow(at: indexPath)?.bounds)!
                presenter.permittedArrowDirections = UIPopoverArrowDirection.unknown
            }
            present(tellAFriendSheet, animated: true, completion:nil)

        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(red: 235.0/255, green: 83.0/255, blue: 122.0/255, alpha: 0.2)
        cell.selectedBackgroundView = backgroundView
    }
    
    /***********************/
    /*** IN-APP PURCHASE ***/
    /***********************/
    
    //NOTIFICATION FROM IAP: We made a request and the server responded in a positive way so we have the product stored in variables. We enable the button and load the price
//    @objc func manageProductResponse() {
//        let formatter = NumberFormatter()
//        formatter.numberStyle = .currency
//        formatter.locale = iAP.removeAdsAndWatermarkProductArray[0].priceLocale
//        let cost = formatter.string(from: iAP.removeAdsAndWatermarkProductArray[0].price)
//
//        if UserDefaults(suiteName: groupNameForExtension)?.bool(forKey: "removeAdsAndWatermarks") == true {
//            enableBuyButton(false)
//            self.removeAdsButton.setTitle(NSLocalizedString("Your app is already Ads/Logo free",comment:"tradurre"), for: .disabled)
//            enableRestoreButton(false)
//        }
//        else {
//            enableBuyButton(true)
//            let title = NSLocalizedString("Remove Ads and Logo (",comment:"tradurre") + cost! + ")"
//            self.removeAdsButton.setTitle(title, for: UIControlState())
//            enableRestoreButton(true)
//        }
//    }
//
//    //ACTION associated to the remove ads/watermarks button
//    @IBAction func removeAdsAndWatermarks(_ sender: UIButton) {
//        //check in the array of all the products until we find the one correspondent to our button
//        for product in iAP.removeAdsAndWatermarkProductArray {
//            let productID = product.productIdentifier
//            //when you get it, buy the product by calling the buyProduct function
//            if productID == iAP.removeAdsAndWatermarksProductIdentifier {
//                iAP.removeAdsAndWatermarkProduct = product
//                iAP.buyProduct()
//                //temporary disable the buy button
//                enableBuyButton(false)
//                break
//            }
//        }
//    }
//
//    //ACTION associated to the restore button. It calls the corresponded function
//    @IBAction func restorePurchases() {
//        iAP.restorePurchases()
//    }
//
//    //Function for removing ads and watermarks. It is called when a buy occur, or when a restore occur. I will save on NSUserDefaults the information about removing ads and watermarks
//    @objc func remove() {
//        print("REMOVE: function to remove the ads")
//        //If a buy occur, or a restore occur, I save this on NSUserDefaults.
//        UserDefaults(suiteName: groupNameForExtension)?.set(true, forKey: "removeAdsAndWatermarks")
//
//        enableBuyButton(false)
//        self.removeAdsButton.setTitle(NSLocalizedString("Your app is already Ads/Logo free",comment:"tradurre"), for: .disabled)
//        enableRestoreButton(false)
//
//        let alert = UIAlertController(title: NSLocalizedString("Well done!",comment:"tradurre"), message: NSLocalizedString("Your app is now ads and Logo free.",comment:"tradurre"), preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:nil))
//        present(alert, animated: true, completion:nil)
//    }
//
//    @objc func transactionFailed() {
//        let alert = iAP.showAlertForFailedTransaction()
//        present(alert, animated: true, completion: {
//            self.enableBuyButton(true)
//            self.enableRestoreButton(true)
//        })
//    }
}
