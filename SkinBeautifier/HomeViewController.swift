//
//  HomeViewController.swift
//  SkinBeautifier
//
//  Created by Md.Ballal Hossen on 25/9/18.
//  Copyright © 2018 GAETANO. All rights reserved.
//

import UIKit
import StoreKit

class HomeViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate,UIDocumentInteractionControllerDelegate, PhotoTweaksViewControllerDelegate {
    

    
    var isCamera = false
    let imagePicker = UIImagePickerController()
    var imageOrientation: UIImageOrientation = .up
    
    /***************************************/
    /*** IAP MANAGER VARIABLES/CONSTANTS ***/
    /***************************************/
    let iAP = InAppPurchase()
    let productIdentifier = inAppProductIdentifier
    var productArray = Array<SKProduct>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Randomly ask the user to leave a review. Managed by apple
        showReview()
        
        
        //set the picker delegate
        imagePicker.delegate = self
        
        //5 - MAKE THE REQUEST FOR THE LIST OF PRODUCTS
        if !iAP.requestProductsData() {
            let alert = iAP.showAlertForIAPDisabled()
            present(alert, animated: true, completion:nil)
        }
        
    }
    
   override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated:false)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    

    @IBAction func galleryButtonAction(_ sender: Any) {
        
        self.imagePicker.allowsEditing = false
        self.imagePicker.navigationBar.barTintColor = UIColor("#131313")
        self.imagePicker.navigationBar.isTranslucent = false
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.navigationBar.tintColor = UIColor.white
        self.imagePicker.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.white
        ]
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func cameraButtonAction(_ sender: Any) {
        
        isCamera = true
        
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .camera
        self.imagePicker.cameraCaptureMode = .photo
        self.imagePicker.modalPresentationStyle = .fullScreen
        self.imagePicker.navigationBar.tintColor = UIColor.black
        self.imagePicker.navigationBar.isTranslucent = false 
        self.present(self.imagePicker, animated: true, completion: nil)
        
    }
    
    @IBAction func moreButtonAction(_ sender: Any) {
    }
    
    @IBAction func removeAdsButtonaction(_ sender: Any) {
        
       
        
        let alert = UIAlertController(title: NSLocalizedString("",comment:""), message: NSLocalizedString("Remove all annoying ads for $3.99 only",comment:""), preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel",comment:""), style: .cancel) { action -> Void in
            
            print("dismiss the sheet")
        }
        
        let buyAction = UIAlertAction(title: NSLocalizedString("Ok",comment:""), style: .default) { action -> Void in
            
            self.iAP.buyProduct()
            
            print("buy Action")
        }
        
        alert.addAction(cancelAction)
        alert.addAction(buyAction)
        
        alert.modalPresentationStyle = .popover
        self.present(alert, animated: true, completion: nil)

    }
    
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        
        picker.dismiss(animated: true) {
            
            if UserDefaults.standard.float(forKey: "saveOriginalPhoto") == 1 && self.isCamera == true{
                
                NSLog("saveOriginalPhoto")
                
                let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage

                UIImageWriteToSavedPhotosAlbum(pickedImage!, nil, nil, nil);
                
                self.isCamera = false
            }
            //Save the orientation of the picked image
            
            self.imageOrientation = (info[UIImagePickerControllerOriginalImage] as? UIImage)!.imageOrientation
            
            //reduce the image size
            let pickedImageReduced = ImageManipulations.reduceImageSize((info[UIImagePickerControllerOriginalImage] as? UIImage)!, maxSizePermitted: 1000)
            
            //re-apply the saved image orientation
            let imageWithCorrectedWithOrientation = UIImage(cgImage: pickedImageReduced.cgImage!, scale: 1.0, orientation: self.imageOrientation)
            
            //Pass everything to the photo tweak library
            self.editPhoto(ImageManipulations.reduceImageSize(imageWithCorrectedWithOrientation, maxSizePermitted: 1000), picker: picker)
            
        }
        
        
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func editPhoto(_ imageFromPicker:UIImage, picker:UIImagePickerController) {
        
        NSLog("T##format: String##String")
        
        let photoTweaksViewController = PhotoTweaksViewController(image: imageFromPicker)
        photoTweaksViewController?.delegate = self
        photoTweaksViewController?.autoSaveToLibray = false
        photoTweaksViewController?.maxRotationAngle = CGFloat(Double.pi/4)
        
        self.navigationController?.pushViewController(photoTweaksViewController as! UIViewController, animated: true)
        
    }
    
    
    // MARK: 2 - PhotoTweaksViewControllerDelegate Methods: SUCCESS
    func photoTweaksController(_ controller: PhotoTweaksViewController!, didFinishWithCroppedImage croppedImage: UIImage!) {
        
//        _ = controller.navigationController?.popViewController(animated: true)
       
        
        
        let destViewController = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        destViewController.sourceImage = croppedImage
        
        controller.navigationController!.pushViewController(destViewController, animated: true)
        
        
    }
    // MARK: 2 - PhotoTweaksViewControllerDelegate Methods: CANCEL
    func photoTweaksControllerDidCancel(_ controller: PhotoTweaksViewController!) {
        //just dismiss the controller
        _ = controller.navigationController?.popViewController(animated: true)
    }
    
    func showReview() {
        let runs = getRunCounts()
        print("Show Review")
        if (runs > minimumRunCount) {
            
            if #available(iOS 10.3, *) {
                print("Review Requested")
                SKStoreReviewController.requestReview()
                
            } else {
                // Fallback on earlier versions
            }
            
        } else {
            print("Runs are not enough to request review!")
        }
    }

}
