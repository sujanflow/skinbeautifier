//
//  BeautiFyViewController.swift
//  SkinBeautifier
//
//  Created by Md.Ballal Hossen on 30/9/18.
//  Copyright © 2018 GAETANO. All rights reserved.
//

import UIKit
import GoogleMobileAds

class BeautiFyViewController: UIViewController,GADBannerViewDelegate,GADInterstitialDelegate {
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var sliderView: UIView!
    
    @IBOutlet weak var hueSlider: GradientSlider!
    
    //for the skin filter
    let context = CIContext(options: [kCIContextWorkingColorSpace: CGColorSpaceCreateDeviceRGB()])
    let filter = YUCIHighPassSkinSmoothing()
    
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    //ADMOB interstitial
    var interstitial: GADInterstitial!

    
    var processedImage: UIImage!
    
    var inputCIImage: CIImage!
    
    var sourceImage: UIImage! {
        didSet {
            //I have to put it in a CIImage for the elaboration
            self.inputCIImage = CIImage(cgImage: self.sourceImage.cgImage!)
        }
    }
    
    var isRemoveAd = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        imageView.image = sourceImage
        
        processedImage = sourceImage
        
        
        isRemoveAd = UserDefaults.standard.bool(forKey: "isRemoveAd")
        
        if !isRemoveAd {
            
            //ADMOB
            interstitial = createAndLoadInterstitial()
            
            
            bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
            bannerView.rootViewController = self
            bannerView.delegate = self
            bannerView.load(GADRequest())
            
        }else{
            
            bannerView.removeFromSuperview()
        }

        self.navigationController?.setNavigationBarHidden(false, animated:false)
        navigationItem.hidesBackButton = true
        
        

    }
    

    @IBAction func sliderChange(_ sender: Any) {
        
        if self.inputCIImage != nil {
            self.processImage()
        }
        
    }
    
    //Core function. It apply the custom filter based on the amount of the slider and save the result in the output variable processedImage
    func processImage() {
        
        self.filter.inputImage = self.inputCIImage
        self.filter.inputAmount = self.hueSlider.value as NSNumber!
        self.filter.inputRadius = NSNumber(value: Float(7.0 * self.inputCIImage.extent.width/750.0))
        self.filter.inputSharpnessFactor = 0.6
        
        
        let outputCIImage = filter.outputImage!
        let outputCGImage = self.context.createCGImage(outputCIImage, from: outputCIImage.extent)
        let outputUIImage = UIImage(cgImage: outputCGImage!, scale: self.sourceImage.scale, orientation: self.sourceImage.imageOrientation)
        self.processedImage = outputUIImage
        
        self.imageView.image = self.processedImage
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
         dismiss(animated: true, completion: nil)
        
        // post a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cancelBeautify"), object: nil, userInfo: nil)
        
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        
        let imageDataDict:[String: UIImage] = ["image": processedImage]
        
        // post a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "beautifyDone"), object: nil, userInfo: imageDataDict)
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    //MARK: 1 - AdMob
    func createAndLoadInterstitial() -> GADInterstitial {
        
        let interstitial = GADInterstitial(adUnitID: admobInterstitialUnitID)
        interstitial.delegate = self
        let request = GADRequest()
        request.testDevices = [testDevice1,testDevice2, testDevice3]
        interstitial.load(request)
        return interstitial
    }
    
    //MARK: 2 - AdMob delegate
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        
        print("interstitialDidReceiveAd")
        
        
        interstitial.present(fromRootViewController: self)
        
        
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        
        // interstitial = createAndLoadInterstitial()
        
    }
}
