//
//  CustomSettingCellTableViewCell.swift
//  SkinBeautifier
//
//  Created by Gaetano La delfa on 20/05/16.
//  Copyright © 2016 GAETANO. All rights reserved.
//

import UIKit

class CustomSettingCellTableViewCell: UITableViewCell {

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textLabel!.font = UIFont(name: fontName, size: fontSize)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
