//
//  MoreViewController.swift
//  SkinBeautifier
//
//  Created by Md.Ballal Hossen on 27/9/18.
//  Copyright © 2018 GAETANO. All rights reserved.
//

import UIKit
import MessageUI

class MoreViewController: UITableViewController,MFMailComposeViewControllerDelegate {
    
    var saveOriginalPhoto = false
    var saveToCustomAlbum = false
    var feedback = false
    // Switches
    @IBOutlet weak var  originalPhotoSwitch:UISwitch!
    @IBOutlet weak var  customAlbumSwitch:UISwitch!
    
    
    // Labels
    @IBOutlet weak var  titleLabel:UILabel!
    @IBOutlet weak var  saveOriginalLabel:UILabel!
    @IBOutlet weak var  saveToCustomAlbumLabel:UILabel!
    @IBOutlet weak var  rateUsLabel:UILabel!
    @IBOutlet weak var  shareAppLabel:UILabel!
    @IBOutlet weak var  sendFeedbackLabel:UILabel!
    @IBOutlet weak var  aboutLabel:UILabel!
    @IBOutlet weak var  likeUsonFBLabel:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.tableFooterView = UIView()
        
        saveOriginalPhoto = UserDefaults.standard.bool(forKey: "saveOriginalPhoto")
        saveToCustomAlbum = UserDefaults.standard.bool(forKey: "saveToCustomAlbum")
        
       NSLog("saveOriginalPhoto = %@", saveOriginalPhoto.description)
        
        
        if saveOriginalPhoto {
            originalPhotoSwitch.isOn = true
        } else {
            originalPhotoSwitch.isOn = false
        }
        
        if saveToCustomAlbum {
            customAlbumSwitch.isOn = true
        } else {
            customAlbumSwitch.isOn = false
        }

        self.navigationController?.navigationBar.isTranslucent = false 
    }
    
    @IBAction func saveToOriginal(_ sender: UISwitch) {
        
        if sender.isOn {
            saveOriginalPhoto = true
        } else {
            saveOriginalPhoto = false
        }
        // Save choice
        UserDefaults.standard.set(saveOriginalPhoto, forKey: "saveOriginalPhoto")
    
    }
    
    
    @IBAction func customAlbum(_ sender: UISwitch) {
        
        if sender.isOn {
            saveToCustomAlbum = true
        } else {
            saveToCustomAlbum = false
        }
        // Save choice
        UserDefaults.standard.set(saveToCustomAlbum, forKey: "saveToCustomAlbum")
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 2:
            
            if let aLINK = URL(string: iTuneLink) {
                UIApplication.shared.openURL(aLINK)
            }
            break
        case 3:
            
            var sharingItems = [AnyHashable]()
            
            sharingItems.append(iTuneLink)
            
            let controller = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
            
            controller.popoverPresentationController?.sourceView = view
            var temp: CGRect = UIScreen.main.bounds
            temp.size.height = temp.size.height / 2
            controller.popoverPresentationController?.sourceRect = temp
            present(controller, animated: true)

                

            
            break
            
        case 4:
            
            feedback = true
            var title = NSLocalizedString("Send feedback", comment: "")
            var message = NSLocalizedString("Please describe  your issues/suggestions below:", comment: "")
            
            if MFMailComposeViewController.canSendMail() {
                print("can send mail")
                
                sendMail(withTitle: title, andMessage: message)
                
            } else {
                print("can not send mail")
                var av = UIAlertView(title: appName, message: NSLocalizedString("Please configure your mail account in device settings.", comment: ""), delegate: nil, cancelButtonTitle: NSLocalizedString("OK", comment: ""))
                av.show()
            }
            
            break

        case 5:
            
            if let aLINK = URL(string: linkToYourPage) {
                
                UIApplication.shared.openURL(aLINK)
            }
            
            break

        default:
            
            break
        }
    }
    
    func sendMail(withTitle title: String?, andMessage message: String?) {
        
        // Allocs the Mail composer controller
        let mc = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(title ?? "")
        mc.setMessageBody(message ?? "", isHTML: false)
        
        if feedback {
            
            let feedbackEmail = [feedbackMail]
            mc.setToRecipients(feedbackEmail)
        }
        
        // Prepare the app Logo to be shared by Email
//        let imageData: Data? = UIImagePNGRepresentation(UIImage(named: "logo"))
//        if let aData = imageData {
//            mc.addAttachmentData(aData, mimeType: "image/png", fileName: "logo.png")
//        }
        
        present(mc, animated: true)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult,
                               error: Swift.Error?) {
        
        controller.dismiss(animated: true, completion: nil)
        
    }

    
    @IBAction func doneButtonAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    

}
