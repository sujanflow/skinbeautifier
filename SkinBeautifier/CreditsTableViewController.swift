//
//  CreditsViewControllerTableViewController.swift
//  SkinBeautifier
//
//  Created by Gaetano La delfa on 20/05/16.
//  Copyright © 2016 GAETANO. All rights reserved.
//

import UIKit

class CreditsTableViewController: UITableViewController {
    
    //The other apps you want to advertise inside your own app. these are the names of my apps. Put the name of yours (they can be more or less)
    let COLORHIT = 0
    let SYMMETRY = 1
    let BABYBIRD = 2
    let BRAINTRAINER = 3
    let NINJAADVENTURE = 4
    let KITOUCH = 5

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //The other apps you want to advertise inside your own app. these are the names of my apps. Put the name of yours (they can be more or less)

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (indexPath as NSIndexPath).section == 2 {
            var url:URL
            switch (indexPath as NSIndexPath).row {
            case COLORHIT:
                url  = URL(string: NSLocalizedString("https://itunes.apple.com/BLABLABLA - your first app", comment:"tradurre"))!
                    UIApplication.shared.openURL(url)
            case SYMMETRY:
                url  =  URL(string: NSLocalizedString("https://itunes.apple.com/BLABLABLA - your second app", comment:"tradurre"))!
                    UIApplication.shared.openURL(url)
            case BABYBIRD:
                url  =  URL(string: NSLocalizedString("https://itunes.apple.com/BLABLABLA - your third app", comment:"tradurre"))!
                    UIApplication.shared.openURL(url)
            case BRAINTRAINER:
                url  =  URL(string: NSLocalizedString("https://itunes.apple.com/BLABLABLA - your fourth app", comment:"tradurre"))!
                    UIApplication.shared.openURL(url)
            case NINJAADVENTURE:
                url  =  URL(string: NSLocalizedString("https://itunes.apple.com/BLABLABLA - your fourth app", comment:"tradurre"))!
                    UIApplication.shared.openURL(url)
            case KITOUCH:
                url  =  URL(string: NSLocalizedString("https://itunes.apple.com/BLABLABLA - your fifth app", comment:"tradurre"))!
                    UIApplication.shared.openURL(url)
            default:
            break;
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(red: 235.0/255, green: 83.0/255, blue: 122.0/255, alpha: 0.2)
        cell.selectedBackgroundView = backgroundView
    }
}
