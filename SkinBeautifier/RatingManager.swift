//
//  RatingManager.swift
//  Symmetry
//
//  Created by Gaetano Carmelo La Delfa on 04/11/2017.
//  Copyright © 2017 LDGEngineering. All rights reserved.
//

import Foundation
import StoreKit

let runIncrementerSetting = "numberOfRuns"  // UserDefauls dictionary key where we store number of runs
let minimumRunCount = 3 // Minimum number of runs that we should have until we ask for review

// counter for number of runs for the app. You can call this from App Delegate
func incrementAppRuns() {
    
    let usD = UserDefaults()
    let runs = getRunCounts() + 1
    usD.setValuesForKeys([runIncrementerSetting: runs])
    usD.synchronize()
    
}

// Reads number of runs from UserDefaults and returns it.
func getRunCounts () -> Int {
    let usD = UserDefaults()
    let savedRuns = usD.value(forKey: runIncrementerSetting)
    
    var runs = 0
    if (savedRuns != nil) {
        
        runs = savedRuns as! Int
    }
    print("Run Counts are \(runs)")
    return runs
}

