//
//  ImageManipulations.swift
//  SkinBeautifier
//
//  Created by Gaetano La delfa on 15/06/16.
//  Copyright © 2016 GAETANO. All rights reserved.
//

import Foundation

class ImageManipulations {
        
    //MARK: add watermark
    class func addWatermark(_ originalImage: UIImage, watermarkImage:UIImage) -> UIImage {
        //1 - create a rect with the same size of the original image
        let rect = CGRect(x: 0, y: 0, width: originalImage.size.width, height: originalImage.size.height)
        
        //2 - Creates a bitmap-based graphics context
        UIGraphicsBeginImageContextWithOptions(originalImage.size, true, 0)
        let context = UIGraphicsGetCurrentContext()
        
        //3 - Paint the area of the rect with the specified color (white)
        context!.setFillColor(UIColor.white.cgColor)
        context!.fill(rect)
        
        //4 - paint the original image onto the rect
        originalImage.draw(in: rect, blendMode: .normal, alpha: 1)
        
        //5 - paint the watermark image onto a specified area
//        watermarkImage.draw(in: CGRect(x: originalImage.size.width*0.01, y: originalImage.size.height - originalImage.size.width/3, width: originalImage.size.width*0.98, height: originalImage.size.width/3), blendMode: .normal, alpha: 0.6)
//        
        //get the result image
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resultImage!
    }
    
    //Reduce the image size in pixels: es from 1000x1000 to 500x500 - taken from NSHipster
    class func reduceImageSize(_ sourceImage:UIImage, maxSizePermitted:Int) -> UIImage {
        //Convert the image in CGImage
        let cgImage = sourceImage.cgImage!
        //Get the Width and Height of the source image
        let originalWidth = cgImage.width
        let originalHeight = cgImage.height
        //Scale factor.
        var factor:Float = 0.0
        
        if max(originalWidth, originalHeight) > maxSizePermitted {
            
            factor = Float(maxSizePermitted) / Float(max(originalWidth, originalHeight))
            let width = Int(Float(cgImage.width) * factor)
            let height = Int(Float(cgImage.height) * factor)
            let bitsPerComponent = cgImage.bitsPerComponent
            let bytesPerRow = cgImage.bytesPerRow
            let colorSpace = cgImage.colorSpace
            //let bitmapInfo = cgImage.bitmapInfo
            //When an image had a transparency the app crashed so I replaced the previous commented line with this. In this way, I remove the transparency (noneSkipLast) and everything works
            let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.noneSkipLast.rawValue)
            
            let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace!, bitmapInfo: bitmapInfo.rawValue)
            
            context!.interpolationQuality = CGInterpolationQuality.high
            
            context?.draw(cgImage, in: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: CGFloat(width), height: CGFloat(height))))
                        
            let scaledImage = context!.makeImage().flatMap { UIImage(cgImage: $0)}
            
            print("scaledImage = \(scaledImage!.imageOrientation.rawValue)")
            return scaledImage!
        }
        else {
            return sourceImage
        }
    }
}

